﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    public static GameController Instance { get; set; }

    [SerializeField] private int _maxBullet = 3;
    private int _currentBullet;

    [SerializeField] private Text _textCountBullet;

    private void Awake () {
        if (Instance == null)
            Instance = this;
        else
            Destroy (gameObject);
    }

    void Start () {
        _currentBullet = _maxBullet;
        _textCountBullet.text = _currentBullet.ToString ();
    }

    void Update () {

    }

    public void Shoot () {
        if (_currentBullet <= 0)
            return;

        _currentBullet--;
        _textCountBullet.text = _currentBullet.ToString ();
    }
}